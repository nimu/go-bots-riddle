module Board exposing (..)

import Css
import Dict exposing (Dict)
import Direction exposing (Direction(..))
import GoBot exposing (GoBot(..), goBotToColorString, goBotToString, stringTogoBot)
import Html.Styled exposing (Html, div)
import Html.Styled.Attributes as Attr
import List.Extra
import Svg.Styled exposing (Svg, circle, g, rect, svg, text, text_)
import Svg.Styled.Attributes as SvgAttr exposing (cx, cy, dominantBaseline, fill, fontSize, height, r, stroke, strokeWidth, style, textAnchor, transform, viewBox, width, x, y)
import Tailwind.Utilities as Tw


type Tile
    = Blank
    | GoBot GoBot
    | Wall
    | Target


type alias Board =
    { width : Int
    , tiles : List Tile
    , goBotPositions : Dict String ( Int, Int )
    , targetPosition : ( Int, Int )
    }


getHeight : Board -> Int
getHeight { width, tiles } =
    List.length tiles // width


getGoBotPosition : Board -> GoBot -> ( Int, Int )
getGoBotPosition board goBot =
    Dict.get (GoBot.goBotToString goBot) board.goBotPositions |> Maybe.withDefault ( -1, -1 )


getTargetPosition : Board -> ( Int, Int )
getTargetPosition board =
    board.targetPosition


viewTile : ( Int, Int ) -> Tile -> Svg msg
viewTile ( row, col ) tile =
    case tile of
        Blank ->
            createSquare ( row, col ) "#ffe6d5"

        GoBot _ ->
            createSquare ( row, col ) "#ffe6d5"

        Wall ->
            createSquare ( row, col ) "#d45500"

        Target ->
            createSquare ( row, col ) "yellow"


squareSize =
    50


createSquare : ( Int, Int ) -> String -> Svg msg
createSquare ( row, col ) color =
    rect
        [ width (String.fromInt squareSize)
        , height (String.fromInt squareSize)
        , fill color
        , x (String.fromInt (col * squareSize))
        , y (String.fromInt (row * squareSize))
        , strokeWidth "1"
        , stroke "black"
        ]
        []


stringToBoard : String -> Int -> Board
stringToBoard string width =
    let
        strToTile : Int -> Char -> ( Int, Tile )
        strToTile idx char =
            case char of
                '.' ->
                    ( idx, Blank )

                'W' ->
                    ( idx, Wall )

                'T' ->
                    ( idx, Target )

                '1' ->
                    ( idx, GoBot One )

                '2' ->
                    ( idx, GoBot Two )

                '3' ->
                    ( idx, GoBot Three )

                '4' ->
                    ( idx, GoBot Four )

                _ ->
                    ( idx, Blank )

        idxTileToBoard : ( Int, Tile ) -> Board -> Board
        idxTileToBoard ( idx, tile ) board =
            let
                ( x, y ) =
                    ( modBy width idx, idx // width )
            in
            case tile of
                Blank ->
                    { board | tiles = List.append board.tiles [ Blank ] }

                GoBot goBot ->
                    { board
                        | goBotPositions = Dict.insert (goBotToString goBot) ( x, y ) board.goBotPositions
                        , tiles = List.append board.tiles [ Blank ]
                    }

                Wall ->
                    { board | tiles = List.append board.tiles [ Wall ] }

                Target ->
                    { board | targetPosition = ( x, y ), tiles = List.append board.tiles [ Target ] }
    in
    string
        |> String.toList
        |> List.indexedMap strToTile
        |> List.foldl idxTileToBoard (Board width [] Dict.empty ( -1, -1 ))


viewBoard : Board -> Html msg
viewBoard board =
    let
        width =
            String.fromInt <|
                board.width
                    * squareSize

        height =
            String.fromInt <|
                (List.length board.tiles // board.width)
                    * squareSize
    in
    div [ Attr.css [ Tw.grid, Tw.grid_cols_1, Tw.items_center, Tw.align_middle, Tw.row_span_6, Tw.overflow_hidden ] ]
        [ svg
            [ viewBox <| "0 0 " ++ width ++ " " ++ height
            , fontSize "2em"
            , SvgAttr.css [ Tw.m_auto, Css.width (Css.pct 100), Css.height (Css.pct 100), Tw.flex_grow ]
            , style "width: 100%;"
            ]
          <|
            viewGrid board
                ++ viewGoBots board
        ]


viewGoBot : ( String, String ) -> GoBot -> Svg msg
viewGoBot ( gbX, gbY ) goBot =
    g
        [ transform <| "translate(" ++ gbX ++ "," ++ gbY ++ ")"
        , style "transition: 1s; transition-timing-function: linear;"
        ]
        [ circle
            [ r (String.fromFloat (squareSize / 2.2))
            , fill <| goBotToColorString goBot
            , cx "0"
            , cy "0"
            , strokeWidth "1"
            , stroke "black"
            ]
            []
        , text_
            [ stroke "white"
            , fill "white"
            , x "0"
            , y "0"
            , textAnchor "middle"
            , dominantBaseline "central"
            ]
            [ text <| goBotToString goBot ]
        ]


idxToRowCol : Int -> Int -> ( Int, Int )
idxToRowCol width index =
    let
        row =
            index // width

        col =
            modBy width index
    in
    Tuple.pair row col


viewGrid : Board -> List (Svg msg)
viewGrid { width, tiles } =
    List.indexedMap (\idx tile -> viewTile (idxToRowCol width idx) tile) tiles


viewGoBots : Board -> List (Svg msg)
viewGoBots { goBotPositions } =
    let
        appender : String -> ( Int, Int ) -> List (Svg msg) -> List (Svg msg)
        appender goBotId ( x, y ) svgs =
            List.append svgs <| viewGoBotAt (stringTogoBot goBotId) ( x, y )
    in
    Dict.foldl appender [] goBotPositions


viewGoBotAt : GoBot -> ( Int, Int ) -> List (Svg msg)
viewGoBotAt goBot ( x, y ) =
    let
        svgX =
            String.fromInt (x * squareSize + squareSize // 2)

        svgY =
            String.fromInt (y * squareSize + squareSize // 2)
    in
    List.singleton <| viewGoBot ( svgX, svgY ) goBot


findArrivalPoint : Board -> ( Int, Int ) -> Direction -> ( Int, Int )
findArrivalPoint board ( x, y ) direction =
    let
        neighbor =
            case direction of
                Up ->
                    ( x, y - 1 )

                Right ->
                    ( x + 1, y )

                Down ->
                    ( x, y + 1 )

                Left ->
                    ( x - 1, y )
    in
    if isOutOfBounds board neighbor || isOccupied board neighbor then
        ( x, y )

    else
        findArrivalPoint board neighbor direction


isOutOfBounds : Board -> ( Int, Int ) -> Bool
isOutOfBounds board ( x, y ) =
    let
        height =
            getHeight board
    in
    x < 0 || x > board.width - 1 || y < 0 || y > height - 1


isOccupied : Board -> ( Int, Int ) -> Bool
isOccupied { tiles, goBotPositions, width } ( x, y ) =
    let
        idx =
            y
                * width
                + x
    in
    case List.Extra.getAt idx tiles of
        Just Wall ->
            True

        _ ->
            Dict.values goBotPositions |> List.member ( x, y )


moveGoBot : GoBot -> Direction -> Board -> Board
moveGoBot goBot direction ({ goBotPositions } as board) =
    let
        goBotId =
            goBotToString goBot

        goBotCurrentPosition =
            Dict.get goBotId goBotPositions
    in
    case goBotCurrentPosition of
        Just currentPosition ->
            { board | goBotPositions = Dict.insert goBotId (findArrivalPoint board currentPosition direction) board.goBotPositions }

        Nothing ->
            board
