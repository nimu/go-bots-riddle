module GoBot exposing (..)

import Css exposing (Color)


type GoBot
    = One
    | Two
    | Three
    | Four


goBotToColor : GoBot -> Color
goBotToColor =
    goBotToColorString >> Css.hex


goBotToColorString : GoBot -> String
goBotToColorString goBot =
    case goBot of
        One ->
            "#5fbcd3"

        Two ->
            "#7137c8"

        Three ->
            "#d40000"

        Four ->
            "#ff6600"


goBotToString : GoBot -> String
goBotToString goBot =
    case goBot of
        One ->
            "1"

        Two ->
            "2"

        Three ->
            "3"

        Four ->
            "4"


stringTogoBot : String -> GoBot
stringTogoBot string =
    if string == "1" then
        One

    else if string == "2" then
        Two

    else if string == "3" then
        Three

    else
        Four
