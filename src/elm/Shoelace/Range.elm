module Shoelace.Range exposing (..)

import Html.Styled as Html exposing (Attribute, Html)
import Html.Styled.Attributes as Attributes
import Html.Styled.Events exposing (on)
import Json.Decode as Decode


slRange : List (Attribute msg) -> List (Html msg) -> Html msg
slRange =
    Html.node "sl-range"



onSlChange : (Int -> msg) -> Attribute msg
onSlChange messageSupplier =
    on "sl-change" (Decode.succeed <| messageSupplier 42)
