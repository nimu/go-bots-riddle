module Main exposing (Flags, Model, Msg(..), init, main, subscriptions, update, view)

-- Imports ---------------------------------------------------------------------

import Board exposing (Board, getGoBotPosition, getTargetPosition, moveGoBot, stringToBoard, viewBoard)
import Browser
import Css exposing (Color, backgroundColor)
import Direction exposing (Direction(..))
import GoBot exposing (GoBot(..), goBotToColor, goBotToString)
import Html.Styled exposing (Html, a, div, p, span)
import Html.Styled.Attributes as Attr
import Html.Styled.Events exposing (onClick)
import List.Extra
import Maybe.Extra
import Process
import Svg.Styled as Svg exposing (Svg, svg, text)
import Svg.Styled.Attributes as SvgAttr exposing (..)
import Tailwind.Theme exposing (gray_300, gray_400, green_400, green_600, red_300, red_400, red_500, red_600, white, yellow_400)
import Tailwind.Utilities as Tw
import Task



-- Main ------------------------------------------------------------------------


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view >> Svg.toUnstyled
        , subscriptions = subscriptions
        }



-- Model -----------------------------------------------------------------------


type alias Flags =
    ()


boardString =
    ""
        ++ "..W..1......W"
        ++ "............."
        ++ "......W4...W."
        ++ "....W.....TW."
        ++ ".........2..."
        ++ "............."
        ++ "W...3........"
        ++ ".......W....."
        ++ ".W.........W."
        ++ "....W...W..W."


goBotBoard : Board
goBotBoard =
    stringToBoard boardString 13


type alias Model =
    { commands : List (Maybe GoBotCommand)
    , board : Board
    , runningCommand : Maybe Int
    , gameOver : Maybe GameOver
    }


type GameOver
    = Won
    | Lost


type alias GoBotCommand =
    { goBot : GoBot
    , direction : Direction
    }


init : Flags -> ( Model, Cmd Msg )
init _ =
    ( { commands = [ Nothing, Nothing, Nothing, Nothing ]
      , board = goBotBoard
      , runningCommand = Nothing
      , gameOver = Nothing
      }
    , Cmd.none
    )



-- Update ----------------------------------------------------------------------


type Msg
    = AddCommand Int
    | NextGoBot Int
    | NextDirection Int
    | DeleteCommand
    | MoveBot Int GoBot Direction
    | HighlightCommand Int
    | Go
    | GameOver
    | Reset


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ({ commands, board } as model) =
    case msg of
        AddCommand idx ->
            ( { model | commands = addCommand idx commands }, Cmd.none )

        NextGoBot idx ->
            ( { model | commands = nextGoBot idx commands }, Cmd.none )

        NextDirection idx ->
            ( { model | commands = nextDirection idx commands }, Cmd.none )

        DeleteCommand ->
            ( { model | commands = deleteCommand commands }, Cmd.none )

        Go ->
            let
                cmdToTask : Int -> GoBotCommand -> List (Cmd Msg)
                cmdToTask idx { goBot, direction } =
                    [ Process.sleep (toFloat (2 * idx * 1000)) |> Task.perform (always (HighlightCommand idx))
                    , Process.sleep (toFloat ((2 * idx + 1) * 1000)) |> Task.perform (always (MoveBot idx goBot direction))
                    ]

                evaluationTask =
                    Process.sleep (toFloat (((2 * List.length model.commands) + 1) * 1000)) |> Task.perform (always GameOver)

                commandTasks =
                    Maybe.Extra.values commands
                        |> List.indexedMap cmdToTask
                        |> List.concat
            in
            ( model, Cmd.batch <| commandTasks ++ [ evaluationTask ] )

        HighlightCommand idx ->
            ( { model | runningCommand = Just idx }, Cmd.none )

        MoveBot idx goBot direction ->
            let
                modifiedBoard =
                    moveGoBot goBot direction board
            in
            ( { model | runningCommand = Just idx, board = modifiedBoard }, Cmd.none )

        GameOver ->
            let
                won =
                    getGoBotPosition board One == getTargetPosition board

                gameOverResult =
                    if won then
                        Won

                    else
                        Lost
            in
            ( { model | runningCommand = Nothing, gameOver = Just gameOverResult }, Cmd.none )

        Reset ->
            ( { model | gameOver = Nothing, board = goBotBoard }, Cmd.none )


next : GoBot -> GoBot
next goBot =
    case goBot of
        One ->
            Two

        Two ->
            Three

        Three ->
            Four

        Four ->
            One


nextD : Direction -> Direction
nextD direction =
    case direction of
        Up ->
            Right

        Right ->
            Down

        Down ->
            Left

        Left ->
            Up


nextGoBot : Int -> List (Maybe GoBotCommand) -> List (Maybe GoBotCommand)
nextGoBot idx goBotCommands =
    let
        mapGoBotCommand : Int -> Int -> Maybe GoBotCommand -> Maybe GoBotCommand
        mapGoBotCommand targetIdx currIdx maybeGoBotCommand =
            case maybeGoBotCommand of
                Just { goBot, direction } ->
                    if targetIdx == currIdx then
                        Just <| GoBotCommand (next goBot) direction

                    else
                        maybeGoBotCommand

                Nothing ->
                    Nothing
    in
    List.indexedMap (mapGoBotCommand idx) goBotCommands


nextDirection : Int -> List (Maybe GoBotCommand) -> List (Maybe GoBotCommand)
nextDirection idx goBotCommands =
    let
        mapGoBotCommand : Int -> Int -> Maybe GoBotCommand -> Maybe GoBotCommand
        mapGoBotCommand targetIdx currIdx maybeGoBotCommand =
            case maybeGoBotCommand of
                Just { goBot, direction } ->
                    if targetIdx == currIdx then
                        Just <| GoBotCommand goBot (nextD direction)

                    else
                        maybeGoBotCommand

                Nothing ->
                    Nothing
    in
    List.indexedMap (mapGoBotCommand idx) goBotCommands


addCommand : Int -> List (Maybe GoBotCommand) -> List (Maybe GoBotCommand)
addCommand idx goBotCommands =
    let
        mapGoBotCommand : Int -> Int -> Maybe GoBotCommand -> Maybe GoBotCommand
        mapGoBotCommand targetIdx currIdx maybeGoBotCommand =
            if targetIdx == currIdx then
                Just <| GoBotCommand One Up

            else
                maybeGoBotCommand
    in
    List.indexedMap (mapGoBotCommand idx) goBotCommands



-- TODO


deleteCommand : List (Maybe GoBotCommand) -> List (Maybe GoBotCommand)
deleteCommand goBotCommands =
    let
        lastJustCommand : Maybe GoBotCommand
        lastJustCommand =
            goBotCommands |> List.reverse |> Maybe.Extra.orList
    in
    case lastJustCommand of
        Just _ ->
            goBotCommands
                |> List.reverse
                |> List.Extra.remove lastJustCommand
                |> (++) [ Nothing ]
                |> List.reverse

        Nothing ->
            goBotCommands



-- View ------------------------------------------------------------------------


view : Model -> Html Msg
view model =
    div [ Attr.css [ Css.width (Css.vw 100), Css.height (Css.vh 100), Tw.flex, Tw.flex_col, Tw.p_6, Tw.box_border ] ] <|
        [ viewControls model
        , viewBoard model.board
        ]
            ++ viewGameOverResult model.gameOver


viewGameOverResult : Maybe GameOver -> List (Html Msg)
viewGameOverResult maybeGameOver =
    let
        wonOpacityList =
            case maybeGameOver of
                Just someGameOver ->
                    if someGameOver == Won then
                        [ Tw.opacity_100
                        , Css.width (Css.vw 100)
                        , Css.height (Css.vh 100)
                        ]

                    else
                        []

                Nothing ->
                    []

        lostOpacityList =
            case maybeGameOver of
                Just someGameOver ->
                    if someGameOver == Lost then
                        [ Tw.opacity_100
                        , Css.width (Css.vw 100)
                        , Css.height (Css.vh 100)
                        ]

                    else
                        []

                Nothing ->
                    []
    in
    [ div
        [ Attr.css <|
            [ Tw.w_0
            , Tw.h_0
            , Tw.grid
            , Tw.grid_cols_1
            , Tw.text_5xl
            , Tw.box_border
            , Tw.align_middle
            , Tw.items_center
            , Tw.justify_items_center
            , Tw.transition_opacity
            , Tw.duration_500
            , Tw.ease_linear
            , Tw.bg_color yellow_400
            , Tw.overflow_hidden
            , Tw.absolute
            , Tw.top_0
            , Tw.left_0
            , Tw.opacity_0
            ]
                ++ wonOpacityList
        ]
        [ text "Gut gemacht!"
        ]
    , div
        [ Attr.css <|
            [ Tw.w_0
            , Tw.h_0
            , Tw.grid
            , Tw.grid_cols_1
            , Tw.text_5xl
            , Tw.box_border
            , Tw.align_middle
            , Tw.items_center
            , Tw.justify_items_center
            , Tw.transition_opacity
            , Tw.duration_1000
            , Tw.ease_linear
            , Tw.bg_color gray_400
            , Tw.overflow_hidden
            , Tw.absolute
            , Tw.text_color white
            , Tw.top_0
            , Tw.left_0
            , Tw.opacity_0
            ]
                ++ lostOpacityList
        ]
        [ p [ css [ Tw.text_center ] ]
            [ span [ css [ Tw.mr_2 ] ] [ text "Schade..." ]
            , span [ css [ Tw.cursor_pointer, Tw.bg_color green_600, Tw.text_color white, Tw.inline_block, Tw.rounded_2xl, Tw.p_4, Tw.self_start ], onClick Reset ] [ text "Neuer Versuch" ]
            ]
        ]
    ]


viewControls : Model -> Html Msg
viewControls model =
    div [ Attr.css [] ]
        [ div [ Attr.css [ Tw.grid, Tw.grid_cols_6, Tw.m_auto, Tw.justify_between, Tw.align_middle, Tw.gap_4, Css.maxWidth (Css.px 600), Tw.mb_2 ] ] <|
            viewCommands model
                ++ viewDeleteButton model
                ++ viewPlayButton model
        ]


viewPlayButton : Model -> List (Html Msg)
viewPlayButton model =
    List.singleton <|
        div
            [ css
                [ Tw.grid
                , Tw.grid_cols_2
                , Tw.content_center
                ]
            ]
        <|
            List.singleton <|
                div
                    [ style "font-size: max(1rem, min(5vw, 2rem))"
                    , css
                        [ Tw.rounded_full
                        , Tw.text_color white
                        , Tw.bg_color green_600
                        , Tw.self_start
                        , Tw.aspect_square
                        , Tw.grid
                        , Tw.grid_cols_1
                        , Tw.items_center
                        , Tw.justify_items_center
                        , Tw.cursor_pointer
                        , Tw.select_none
                        , Tw.relative
                        ]
                    , onClick Go
                    ]
                    [ viewPlayIcon ]


viewDeleteButton : Model -> List (Html Msg)
viewDeleteButton { commands } =
    let
        disabled =
            List.isEmpty commands
    in
    List.singleton <|
        div
            [ css
                [ Tw.grid
                , Tw.w_full
                , Tw.grid_cols_2
                , Tw.content_center
                ]
            ]
            [ div [] []
            , div
                [ style "font-size: max(1rem, min(5vw, 2rem))"
                , css
                    [ Tw.rounded_2xl
                    , Tw.text_color white
                    , Tw.bg_color <|
                        if disabled then
                            gray_400

                        else
                            red_600
                    , Tw.self_start
                    , Tw.aspect_square
                    , Tw.grid
                    , Tw.grid_cols_1
                    , Tw.items_center
                    , Tw.justify_items_center
                    , Tw.cursor_pointer
                    , Tw.select_none
                    , Tw.relative
                    ]
                , onClick <| DeleteCommand
                ]
                [ viewDeleteIcon ]
            ]


viewCommands : Model -> List (Svg Msg)
viewCommands ({ commands, board, runningCommand } as model) =
    List.indexedMap (viewMaybeCommand model) commands


viewMaybeCommand : Model -> Int -> Maybe GoBotCommand -> Html Msg
viewMaybeCommand { commands, board, runningCommand } idx maybeGoBotCommand =
    case maybeGoBotCommand of
        Just someGoBotCommand ->
            viewGoBotCommand runningCommand idx someGoBotCommand

        Nothing ->
            viewEmptyGoBotCommand (List.Extra.findIndex Maybe.Extra.isNothing commands) idx


viewGoBotCommand : Maybe Int -> Int -> GoBotCommand -> Html Msg
viewGoBotCommand runningCommand idx { goBot, direction } =
    let
        goBotColor =
            goBotToColor goBot

        goBotNr =
            goBotToString goBot

        currentlyRunning =
            case runningCommand of
                Just idxOfRunning ->
                    idxOfRunning == idx

                Nothing ->
                    False
    in
    div
        [ css
            [ Tw.grid
            , Tw.grid_cols_2
            , Tw.content_center
            , Tw.relative
            ]
        ]
    <|
        [ div
            -- GO Bot Number
            [ css
                [ backgroundColor goBotColor
                , Tw.rounded_l_2xl
                , Tw.text_color white
                , Tw.grid
                , Tw.items_center
                , Tw.justify_items_center
                , Tw.cursor_pointer
                , Tw.select_none
                ]
            , style "font-size: max(1rem, min(5vw, 2rem))"
            , onClick (NextGoBot idx)
            ]
            [ text goBotNr
            ]
        , div
            -- Direction
            [ css
                [ Tw.bg_color gray_300
                , Tw.rounded_r_2xl
                , Tw.text_4xl
                , Tw.grid
                , Tw.items_center
                , Tw.justify_items_center
                , Tw.cursor_pointer
                , Tw.select_none
                , Tw.h_full
                ]
            , onClick (NextDirection idx)
            ]
            [ viewDirection direction
            ]
        ]
            ++ (if currentlyRunning then
                    [ span
                        [ css
                            [ Tw.animate_ping
                            , Tw.absolute
                            , Tw.inline_flex
                            , Tw.h_full
                            , Tw.w_full
                            , Tw.rounded_full
                            , backgroundColor goBotColor
                            , Tw.opacity_20
                            ]
                        ]
                        []
                    ]

                else
                    []
               )


viewEmptyGoBotCommand : Maybe Int -> Int -> Html Msg
viewEmptyGoBotCommand maybeFirstEmpty idx =
    let
        firstEmpty =
            case maybeFirstEmpty of
                Just someFirstEmpty ->
                    someFirstEmpty == idx

                Nothing ->
                    False
    in
    if firstEmpty then
        div
            [ css
                [ Tw.flex_none
                , Tw.grid
                , Tw.grid_cols_2
                , Tw.content_center
                , Tw.relative
                ]
            ]
            [ div
                -- GO Bot Number
                [ css
                    [ Tw.bg_color gray_400
                    , Tw.rounded_2xl
                    , Tw.text_color white
                    , Tw.aspect_square
                    , Tw.grid
                    , Tw.items_center
                    , Tw.justify_items_center
                    , Tw.cursor_pointer
                    , Tw.select_none
                    ]
                , style "font-size: max(1rem, min(5vw, 2rem))"
                , onClick (AddCommand idx)
                ]
                [ text "+"
                ]
            ]

    else
        div [] []


viewDirection : Direction -> Svg Msg
viewDirection direction =
    let
        rotation =
            case direction of
                Up ->
                    "rotate(0)"

                Right ->
                    "rotate(90deg)"

                Down ->
                    "rotate(180deg)"

                Left ->
                    "rotate(270deg)"
    in
    svg
        [ SvgAttr.xmlSpace "preserve"
        , SvgAttr.viewBox "0 0 21.580782 29.448362"
        , height "50%"
        , width "50%"
        , SvgAttr.stroke "currentColor"
        , SvgAttr.style <| "transition: transform: 0.5s; transform: " ++ rotation
        ]
        [ Svg.path
            [ SvgAttr.d "M80.780956 100.4233v-6.997547m0 0-2.457958 2.457958m4.915916 0-2.457958-2.457958"
            , SvgAttr.style "fill:#007118;stroke-width:.793999;stroke-linecap:round"
            , SvgAttr.transform <| "translate(-294.52346 -351.60474) scale(3.7795) "
            ]
            []
        ]


viewDeleteIcon : Svg msg
viewDeleteIcon =
    svg
        [ SvgAttr.fill "none"
        , SvgAttr.viewBox "0 0 24 24"
        , SvgAttr.strokeWidth "1.5"
        , SvgAttr.stroke "currentColor"
        , SvgAttr.css [ Tw.w_1over2 ]
        ]
        [ Svg.path
            [ SvgAttr.strokeLinecap "round"
            , SvgAttr.strokeLinejoin "round"
            , SvgAttr.d "m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0"
            ]
            []
        ]


viewPlayIcon : Svg msg
viewPlayIcon =
    svg
        [ SvgAttr.fill "none"
        , SvgAttr.viewBox "0 0 24 24"
        , SvgAttr.strokeWidth "1.5"
        , SvgAttr.stroke "currentColor"
        , SvgAttr.css [ Tw.w_1over2 ]
        ]
        [ Svg.path
            [ SvgAttr.strokeLinecap "round"
            , SvgAttr.strokeLinejoin "round"
            , SvgAttr.d "M5.25 5.653c0-.856.917-1.398 1.667-.986l11.54 6.347a1.125 1.125 0 0 1 0 1.972l-11.54 6.347a1.125 1.125 0 0 1-1.667-.986V5.653Z"
            ]
            []
        ]



-- Subscriptions ---------------------------------------------------------------


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none
